import * as React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

export interface Props {
  name?: string;
  email?: string;
  password?: string;
  repassword?: string;
  error_username?: string;
  error_email?: string;
  error_password?: string;
  error_repassword?: string;
}

export interface Object {
  username?: string;
  email?: string;
  password?: string;
  repassword?: string;
  error_username?: string;
  error_email?: string;
  error_password?: string;
  error_repassword?: string;
}

let object: Array<string>;

class Login extends React.Component<Props, Object> {

  constructor(props: Props) {
    super(props);
    const {name, email, password, repassword, error_email, error_password, error_repassword,
      error_username} = this.props;

    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangeUserName = this.handleChangeUserName.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeRePassword = this.handleChangeRePassword.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event.target.value + '');
    // this.props.email = event.target.value;
    // console.log(this.props.email);
    // this.setState({ email: (event.target as HTMLInputElement).value });
    // if (this.state.email.length < 10) {
    //   this.state.error_email = 'Please enter the correct mail';
    // }
  }

  handleChangeUserName(event: KeyboardEvent) {
    this.setState({ username: (event.target as HTMLInputElement).value });
    // if (this.state.username.length < 8) {
    //   this.state.error_email = 'Username Should be atleast 8 charectars';
    // } else {
    //   this.state.error_email = 'Correct';
    // }
  }

  handleChangePassword(event: KeyboardEvent) {
    this.setState({ password: (event.target as HTMLInputElement).value });
  }

  handleChangeRePassword(event: KeyboardEvent) {
    this.setState({ repassword: (event.target as HTMLInputElement).value });
  }

  handleSubmit(event: MouseEvent) {
    // alert('A name was submitted: ' + this.state.value);
    // event.preventDefault();
  }
  render() {
    return (
      <div className="App">
        <form>
          <Grid>
            <Row className="show-grid">
              <Col className="right" md={2}>
                <label className="label-body">
                  Email
                </label>
              </Col>
              <Col className="left" md={10}>
                <input type="email" value={this.props.email} onChange={this.handleChangeEmail} name="email" />
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="left" mdOffset={2} md={10}>
                <p>some text</p>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="right" md={2}>
                <label className="label-body">
                  User Name
                </label>
              </Col>
              <Col className="left" md={10}>
                <input type="text" name="name" />
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="left" mdOffset={2} md={10}>
                <p>some text</p>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="right" md={2}>
                <label className="label-body">
                  Password
                </label>
              </Col>
              <Col className="left" md={10}>
                <input type="password" name="password" />
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="left" mdOffset={2} md={10}>
                <p>some text</p>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="right" md={2}>
                <label className="label-body">
                  Confirm Password
                </label>
              </Col>
              <Col className="left" md={10}>
                <input type="password" name="confirmPassword" />
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="left" mdOffset={2} md={10}>
                <p>some text</p>
              </Col>
            </Row>
            <Row className="show-grid">
              <Col className="left" mdOffset={2} md={10}>
                <input type="submit" value="Submit" />
              </Col>
            </Row>

          </Grid>
        </form>
      </div>
    );
  }
}

// function Login({ name, enthusiasmLevel = 1 }: Props) {
//   if (enthusiasmLevel <= 0) {
//     throw new Error('You could be a little more enthusiastic. :D');
//   }

//   return (

//   );
// }

export default Login;

// // helpers

// function getExclamationMarks(numChars: number) {
//   return Array(numChars + 1).join('!');
// }