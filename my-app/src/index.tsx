import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Login from './components/Login';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

ReactDOM.render(
  <Login />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
